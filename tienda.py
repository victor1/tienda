import sys

articulos = {}

for i, arg in enumerate(sys.argv[1:]):
    if i % 2 == 0:
        articulos[arg] = None
    else:
        articulos[list(articulos.keys())[-1]] = float(arg)

if len(sys.argv[1:]) % 2 != 0:
    print("Error: Número impar de argumentos")
    ValueError
try:
    articulos[list(articulos.keys())[-1]] = float(arg)
except ValueError:
    print(f"Error: No se pudo convertir '{arg}' a float")
if not isinstance(arg, str):
    print(f"Error: La llave '{arg}' no es un string")


def anadir(nombre, precio):
    articulos[nombre] = precio


def mostrar():
    print("Lista de artículos:",sys.argv[0:])
    for nombre, precio in articulos.items():
        print(f" {nombre}: {precio} euros")


def pedir_articulo():
    articulo = input("¿Qué artículo deseas comprar? ")
    while articulo not in articulos:
        print("Artículo no encontrado.")
        articulo = input("¿Qué artículo deseas comprar? ")

    return articulo


def pedir_cantidad():
    while True:
        try:
            cantidad = float(input("¿Cuántas unidades deseas comprar? "))
            return cantidad
        except ValueError:
            print("Introduce un número válido")


def main():
    terminar_compra = False
    suma = 0
    total = 0
    while not terminar_compra:
        mostrar()
        articulo = pedir_articulo()
        cantidad = pedir_cantidad()
        x = input("deseas continuar tu compra? ")
        if x .lower() != "no":
            print(f"\nHas comprado {cantidad} unidades de {articulo}")
            suma = articulos[articulo] * cantidad + suma
            total += suma
            print("a pagar: ",total)
        elif x .lower() =="no":
            terminar_compra = True
            print(f"\nHas comprado {cantidad} unidades de {articulo}")
            total = total + articulos[articulo] * cantidad
            print("a pagar: ",total)



if __name__ == "__main__":
    main()
